package gr_rbac

import (
	"context"
	"reflect"
	"testing"
)

func Test_accessChecker_CheckAccess(t *testing.T) {
	permission := "test"

	type fields struct {
		permissionsGetter UserPermissionsGetterInterface
		groupsGetter      GroupGetterInterface
	}
	type args struct {
		ctx                  context.Context
		userIdentifier       string
		permissionIdentifier *string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Разрешение не передано, ошибки нет",
			fields: fields{
				permissionsGetter: userPermissionsGetterMock{
					permissions: []UserPermissionInterface{
						userPermissionMock{
							group:      "1",
							permission: "test",
						},
					},
					isError: false,
				},
				groupsGetter: groupGetterMock{
					isError: false,
				},
			},
			args: args{
				ctx:                  nil,
				userIdentifier:       "test user",
				permissionIdentifier: nil,
			},
			wantErr: false,
		},
		{
			name: "Разрешение передано, получить разрешения пользователя не удалось, должна быть ошибка доступа",
			fields: fields{
				permissionsGetter: userPermissionsGetterMock{
					permissions: []UserPermissionInterface{
						userPermissionMock{
							group:      "1",
							permission: "test",
						},
					},
					isError: true,
				},
				groupsGetter: groupGetterMock{
					isError: false,
				},
			},
			args: args{
				ctx:                  nil,
				userIdentifier:       "test user",
				permissionIdentifier: &permission,
			},
			wantErr: true,
		},
		{
			name: "Разрешение передано, разрешения получены, не найдено запрошенное, должна быть ошибка доступа",
			fields: fields{
				permissionsGetter: userPermissionsGetterMock{
					permissions: []UserPermissionInterface{
						userPermissionMock{
							group:      "1",
							permission: "test 2",
						},
					},
					isError: false,
				},
				groupsGetter: groupGetterMock{
					isError: false,
				},
			},
			args: args{
				ctx:                  nil,
				userIdentifier:       "test user",
				permissionIdentifier: &permission,
			},
			wantErr: true,
		},
		{
			name: "Разрешение передано, разрешения получены, найдено запрошенное, ошибок нет",
			fields: fields{
				permissionsGetter: userPermissionsGetterMock{
					permissions: []UserPermissionInterface{
						userPermissionMock{
							group:      "1",
							permission: "test",
						},
					},
					isError: false,
				},
				groupsGetter: groupGetterMock{
					isError: false,
				},
			},
			args: args{
				ctx:                  nil,
				userIdentifier:       "test user",
				permissionIdentifier: &permission,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := accessChecker{
				permissionsGetter: tt.fields.permissionsGetter,
				groupsGetter:      tt.fields.groupsGetter,
			}
			if err := a.CheckAccess(tt.args.ctx, tt.args.userIdentifier, tt.args.permissionIdentifier); (err != nil) != tt.wantErr {
				t.Errorf("CheckAccess() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_accessChecker_CheckAccessAndGetGroupsAvailableForUser(t *testing.T) {
	permission := "test"

	type fields struct {
		permissionsGetter UserPermissionsGetterInterface
		groupsGetter      GroupGetterInterface
	}
	type args struct {
		ctx                  context.Context
		userIdentifier       string
		permissionIdentifier *string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		{
			name: "Разрешение не передано, ошибок нет, вернулись все группы",
			fields: fields{
				permissionsGetter: userPermissionsGetterMock{
					permissions: []UserPermissionInterface{
						userPermissionMock{
							group:      "1",
							permission: "test",
						},
						userPermissionMock{
							group:      "2",
							permission: "test 2",
						},
					},
					isError: false,
				},
				groupsGetter: groupGetterMock{
					isError: false,
				},
			},
			args: args{
				ctx:                  nil,
				userIdentifier:       "test user",
				permissionIdentifier: nil,
			},
			want:    []string{"1", "2"},
			wantErr: false,
		},
		{
			name: "Разрешение передано, не удалось получить разрешения пользователя, должна быть ошибка",
			fields: fields{
				permissionsGetter: userPermissionsGetterMock{
					permissions: []UserPermissionInterface{
						userPermissionMock{
							group:      "1",
							permission: "test",
						},
						userPermissionMock{
							group:      "2",
							permission: "test 2",
						},
					},
					isError: true,
				},
				groupsGetter: groupGetterMock{
					isError: false,
				},
			},
			args: args{
				ctx:                  nil,
				userIdentifier:       "test user",
				permissionIdentifier: &permission,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Разрешение передано, разрешения пользователя получены, не найдено запрошенное разрешение, ошибка доступа",
			fields: fields{
				permissionsGetter: userPermissionsGetterMock{
					permissions: []UserPermissionInterface{
						userPermissionMock{
							group:      "1",
							permission: "test 3",
						},
						userPermissionMock{
							group:      "2",
							permission: "test 2",
						},
					},
					isError: false,
				},
				groupsGetter: groupGetterMock{
					isError: false,
				},
			},
			args: args{
				ctx:                  nil,
				userIdentifier:       "test user",
				permissionIdentifier: &permission,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Разрешение передано, разрешения пользователя получены, найдено требуемое разрешение, не удалось получить группы, ошибка доступа",
			fields: fields{
				permissionsGetter: userPermissionsGetterMock{
					permissions: []UserPermissionInterface{
						userPermissionMock{
							group:      "1",
							permission: "test",
						},
						userPermissionMock{
							group:      "2",
							permission: "test 2",
						},
					},
					isError: false,
				},
				groupsGetter: groupGetterMock{
					isError: true,
				},
			},
			args: args{
				ctx:                  nil,
				userIdentifier:       "test user",
				permissionIdentifier: &permission,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Разрешение передано, разрешения пользователя получены, найдено требуемое, группы получены, ошибок нет",
			fields: fields{
				permissionsGetter: userPermissionsGetterMock{
					permissions: []UserPermissionInterface{
						userPermissionMock{
							group:      "1",
							permission: "test",
						},
						userPermissionMock{
							group:      "2",
							permission: "test",
						},
						userPermissionMock{
							group:      "3",
							permission: "test 3",
						},
					},
					isError: false,
				},
				groupsGetter: groupGetterMock{
					isError: false,
				},
			},
			args: args{
				ctx:                  nil,
				userIdentifier:       "test user",
				permissionIdentifier: &permission,
			},
			want:    []string{"1", "2"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := accessChecker{
				permissionsGetter: tt.fields.permissionsGetter,
				groupsGetter:      tt.fields.groupsGetter,
			}
			got, err := a.CheckAccessAndGetGroupsAvailableForUser(tt.args.ctx, tt.args.userIdentifier, tt.args.permissionIdentifier)
			if (err != nil) != tt.wantErr {
				t.Errorf("CheckAccessAndGetGroupsAvailableForUser() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CheckAccessAndGetGroupsAvailableForUser() got = %v, want %v", got, tt.want)
			}
		})
	}
}
