package gr_rbac

import "context"

// Базовый геттер групп. Используется в случаях, когда структура групп линейна, а не древовидна.
type baseGroupGetter struct{}

// Конструктор геттера
func newBaseGroupGetter() GroupGetterInterface {
	return &baseGroupGetter{}
}

// Получение вложенных групп по переданным ID родителей.
//
// По сути в данной библиотеке учитывается, что группы могут иметь вложенную (древовидную) структуру,
// поэтому для доступных напрямую пользовательских групп необходимо так же получить вложенные подгруппы.
// Данный метод как раз таки возвращает список подгрупп, доступных пользователю.
func (b baseGroupGetter) GetNestedGroupsByIds(ctx context.Context, groupId []string) (nestedGroups []string, err error) {
	return groupId, nil
}
