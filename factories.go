package gr_rbac

// Фабрика чекера с использованием дефолтного геттера групп
// Данный чекер подходит тогда, когда группы расположены линейно.
func NewAccessChecker(permissionsGetter UserPermissionsGetterInterface) AccessCheckerInterface {
	return &accessChecker{
		permissionsGetter: permissionsGetter,
		groupsGetter:      newBaseGroupGetter(),
	}
}

// Фабрика чекера с использованием кастомного геттера групп.
// В фабрику передается реализация геттера групп, который будет использоваться в чекере.
func NewAccessCheckerWithGroupGetter(
	permissionsGetter UserPermissionsGetterInterface,
	groupsGetter GroupGetterInterface,
) AccessCheckerInterface {
	return &accessChecker{
		permissionsGetter: permissionsGetter,
		groupsGetter:      groupsGetter,
	}
}
