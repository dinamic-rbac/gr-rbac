package gr_rbac

import (
	"context"

	"github.com/pkg/errors"
)

// Сервис проверки доступа для переданных пользователей
type accessChecker struct {
	permissionsGetter UserPermissionsGetterInterface
	groupsGetter      GroupGetterInterface
}

// GetUserPermissions выполняет получение всех доступных для пользователя разрешений по
// переданному идентификатору в результате должен вернуться массив разрешений или ошибка
// их получения.
func (a accessChecker) GetUserPermissions(ctx context.Context, userIdentifier string) (permissions []UserPermissionInterface, err error) {
	return a.permissionsGetter.GetUserPermissions(ctx, userIdentifier)
}

// CheckAccessAndGetGroupsAvailableForUser - Метод позволяет проверить наличие у
// пользователя разрешения, а так же получить список открытых для этого разрешения групп.
//
// Если у пользователя нет необходимых разрешений, то возвращается ошибка доступа.
// Если разрешение не переданно, то это будет означать, что проверка не требуется.
//
// Простой пример:
// У пользователя есть разрешение А для группы 1 и разрешение Б для групп 1 и 2,
// при этом этот метод, при передаче в него идентификатора пользователя и разрешения А,
// вернет только группу 1, а при передаче разрешения Б - обе группы.
func (a accessChecker) CheckAccessAndGetGroupsAvailableForUser(
	ctx context.Context,
	userIdentifier string,
	permissionIdentifier *string,
) ([]string, error) {
	var identifiers []string
	if nil != permissionIdentifier {
		identifiers = []string{*permissionIdentifier}
	}

	return a.CheckPermissionsAndGetGroupsAvailableForUser(ctx, userIdentifier, identifiers)
}

// CheckPermissionsAndGetGroupsAvailableForUser - Метод позволяет проверить наличие у
// пользователя разрешений, а так же получить список открытых для этих разрешений групп.
//
// Если у пользователя нет необходимых разрешений, то возвращается ошибка доступа.
// Если разрешение не передано, то это будет означать, что проверка не требуется.
//
// Простой пример:
// У пользователя есть разрешение А для группы 1 и разрешение Б для группы 2,
// при этом этот метод, при передаче в него идентификаторов разрешений А и Б,
// вернет группы 1 и 2.
func (a accessChecker) CheckPermissionsAndGetGroupsAvailableForUser(
	ctx context.Context,
	userIdentifier string,
	permissions []string,
) ([]string, error) {
	userPermissions, err := a.getAvailablePermissionsForUser(ctx, userIdentifier, permissions)
	if nil != err {
		return nil, err
	}

	groups := make([]string, 0, len(userPermissions))
	for _, permission := range userPermissions {
		if a.isGroupAlreadyInArray(permission.GetGroup(), groups) {
			continue
		}

		groups = append(groups, permission.GetGroup())
	}

	result, err := a.groupsGetter.GetNestedGroupsByIds(ctx, groups)
	if nil != err {
		return nil, errors.Wrap(err, `user has not access (failed to get nested groups)`)
	}

	return result, nil
}

// CheckAccess Метод позволяет проверить наличие у пользователя переданного разрешения
// и на основе этого сделать вывод о наличии у пользователя доступа.
//
// Если у пользователя нет необходимых разрешений, то возвращается ошибка доступа.
// Если разрешение не передано, то это будет означать, что проверка не требуется.
//
// Метод не проверяет для какой из групп установлено разрешение. Метод проверяет только
// само наличие разрешения у пользователя
func (a accessChecker) CheckAccess(
	ctx context.Context,
	userIdentifier string,
	permissionIdentifier *string,
) error {
	if nil == permissionIdentifier {
		return nil
	}

	return a.CheckPermissions(ctx, userIdentifier, []string{*permissionIdentifier})
}

// CheckPermissions Метод позволяет проверить наличие у пользователя переданных разрешений
// и на основе этого сделать вывод о наличии у пользователя доступа.
//
// Если у пользователя нет необходимых разрешений, то возвращается ошибка доступа.
// Если разрешение не переданно, то это будет означать, что проверка не требуется.
//
// Метод не проверяет для какой из групп установлено разрешение. Метод проверяет только
// само наличие разрешения у пользователя
func (a accessChecker) CheckPermissions(
	ctx context.Context,
	userIdentifier string,
	permissions []string,
) error {
	if 0 == len(permissions) {
		return nil
	}

	userPermissions, err := a.permissionsGetter.GetUserPermissions(ctx, userIdentifier)
	if nil != err {
		return errors.Wrap(err, `failed to get user permission`)
	}

	for _, permission := range userPermissions {
		if a.inArray(permission.GetPermission(), permissions) {
			return nil
		}
	}

	return errors.New(`user has not access (permission is not allowed for user)`)
}

// Проверяет наличие переданной группы в массиве
func (a accessChecker) isGroupAlreadyInArray(group string, groups []string) bool {
	for _, available := range groups {
		if available == group {
			return true
		}
	}

	return false
}

// Получение списка доступных разрешений для переданного пользователя
func (a accessChecker) getAvailablePermissionsForUser(
	ctx context.Context,
	userIdentifier string,
	permissionIdentifier []string,
) ([]UserPermissionInterface, error) {
	permissions, err := a.permissionsGetter.GetUserPermissions(ctx, userIdentifier)
	if nil != err {
		return nil, errors.Wrap(err, `failed to get user permission`)
	}

	if 0 == len(permissionIdentifier) {
		return permissions, nil
	}

	result := make([]UserPermissionInterface, 0, len(permissions))
	for _, permission := range permissions {
		if !a.inArray(permission.GetPermission(), permissionIdentifier) {
			continue
		}

		result = append(result, permission)
	}

	if 0 == len(result) {
		return nil, errors.New(`user has not access (permission is not allowed for user)`)
	}

	return result, nil
}

// inArray проверяет наличие переданной строки в массиве
func (a accessChecker) inArray(val string, array []string) bool {
	for _, exists := range array {
		if exists == val {
			return true
		}
	}

	return false
}
