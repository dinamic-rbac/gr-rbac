package gr_rbac

import (
	"context"
	"reflect"
	"testing"
)

func Test_baseGroupGetter_GetNestedGroupsByIds(t *testing.T) {
	type args struct {
		ctx     context.Context
		groupId []string
	}
	tests := []struct {
		name             string
		args             args
		wantNestedGroups []string
		wantErr          bool
	}{
		{
			name: `Тестирование получения групп. Должны вернуться те, что переданы сверху`,
			args: args{
				ctx:     nil,
				groupId: []string{"1", "2"},
			},
			wantNestedGroups: []string{"1", "2"},
			wantErr:          false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := baseGroupGetter{}
			gotNestedGroups, err := b.GetNestedGroupsByIds(tt.args.ctx, tt.args.groupId)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetNestedGroupsByIds() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotNestedGroups, tt.wantNestedGroups) {
				t.Errorf("GetNestedGroupsByIds() gotNestedGroups = %v, want %v", gotNestedGroups, tt.wantNestedGroups)
			}
		})
	}
}
