package gr_rbac

import (
	"context"
	"errors"
)

// Подставка для тестирования
type userPermissionsGetterMock struct {
	permissions []UserPermissionInterface
	isError     bool
}

// Получение всех доступных для пользователя разрешений по переданному идентификатору
// В результате должен вернуться массив разрешений или ошибка их получения.
func (u userPermissionsGetterMock) GetUserPermissions(ctx context.Context, userIdentifier string) (permissions []UserPermissionInterface, err error) {
	if u.isError {
		return nil, errors.New(`test error`)
	}

	return u.permissions, nil
}
