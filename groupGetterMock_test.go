package gr_rbac

import (
	"context"
	"errors"
)

// Подставка для тестирования
type groupGetterMock struct {
	isError bool
}

// Получение вложенных групп по переданным ID родителей.
//
// По сути в данной библиотеке учитывается, что группы могут иметь вложенную (древовидную) структуру,
// поэтому для доступных напрямую пользовательских групп необходимо так же получить вложенные подгруппы.
// Данный метод как раз таки возвращает список подгрупп, доступных пользователю.
func (g groupGetterMock) GetNestedGroupsByIds(ctx context.Context, groupId []string) (nestedGroups []string, err error) {
	if g.isError {
		return nil, errors.New(`test error`)
	}

	return groupId, nil
}
