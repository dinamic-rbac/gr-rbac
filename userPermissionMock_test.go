package gr_rbac

// Подставка для тестирования
type userPermissionMock struct {
	group      string
	permission string
}

// Получение группы, для которой открыто разрешение
func (u userPermissionMock) GetGroup() string {
	return u.group
}

// Получение идентификатора разрешения
func (u userPermissionMock) GetPermission() string {
	return u.permission
}
