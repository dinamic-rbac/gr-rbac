package gr_rbac

import "context"

// AccessCheckerInterface описывает интерфейс сервиса проверки доступа для переданных пользователей
type AccessCheckerInterface interface {
	UserPermissionsGetterInterface

	// CheckAccessAndGetGroupsAvailableForUser - Метод позволяет проверить наличие у
	// пользователя разрешения, а так же получить список открытых для этого разрешения групп.
	//
	// Если у пользователя нет необходимых разрешений, то возвращается ошибка доступа.
	// Если разрешение не передано, то это будет означать, что проверка не требуется.
	//
	// Простой пример:
	// У пользователя есть разрешение А для группы 1 и разрешение Б для групп 1 и 2,
	// при этом этот метод, при передаче в него идентификатора пользователя и разрешения А,
	// вернет только группу 1, а при передаче разрешения Б - обе группы.
	CheckAccessAndGetGroupsAvailableForUser(
		ctx context.Context,
		userIdentifier string,
		permissionIdentifier *string,
	) ([]string, error)

	// CheckPermissionsAndGetGroupsAvailableForUser - Метод позволяет проверить наличие у
	// пользователя разрешений, а так же получить список открытых для этих разрешений групп.
	//
	// Если у пользователя нет необходимых разрешений, то возвращается ошибка доступа.
	// Если разрешение не передано, то это будет означать, что проверка не требуется.
	//
	// Простой пример:
	// У пользователя есть разрешение А для группы 1 и разрешение Б для группы 2,
	// при этом этот метод, при передаче в него идентификаторов разрешений А и Б,
	// вернет группы 1 и 2.
	CheckPermissionsAndGetGroupsAvailableForUser(
		ctx context.Context,
		userIdentifier string,
		permissions []string,
	) ([]string, error)

	// CheckAccess Метод позволяет проверить наличие у пользователя переданного разрешения
	// и на основе этого сделать вывод о наличии у пользователя доступа.
	//
	// Если у пользователя нет необходимых разрешений, то возвращается ошибка доступа.
	// Если разрешение не передано, то это будет означать, что проверка не требуется.
	//
	// Метод не проверяет для какой из групп установлено разрешение. Метод проверяет только
	// само наличие разрешения у пользователя
	CheckAccess(
		ctx context.Context,
		userIdentifier string,
		permissionIdentifier *string,
	) error

	// CheckPermissions Метод позволяет проверить наличие у пользователя любого из
	// переданных разрешений и на основе этого сделать вывод о наличии у пользователя
	// доступа.
	//
	// Если у пользователя нет необходимых разрешений, то возвращается ошибка доступа.
	// Если разрешение не переданно, то это будет означать, что проверка не требуется.
	//
	// Метод не проверяет для какой из групп установлено разрешение. Метод проверяет только
	// само наличие разрешения у пользователя
	CheckPermissions(
		ctx context.Context,
		userIdentifier string,
		permissions []string,
	) error
}

// UserPermissionInterface - Интерфейс разрешения пользователя
type UserPermissionInterface interface {
	// GetGroup - Получение группы, для которой открыто разрешение
	GetGroup() string

	// GetPermission - Получение идентификатора разрешения
	GetPermission() string
}

// UserPermissionsGetterInterface - Геттер для получения доступных пользователю разрешений
type UserPermissionsGetterInterface interface {
	// GetUserPermissions выполняет получение всех доступных для пользователя разрешений по
	// переданному идентификатору в результате должен вернуться массив разрешений или ошибка
	// их получения.
	GetUserPermissions(ctx context.Context, userIdentifier string) (permissions []UserPermissionInterface, err error)
}

// GroupGetterInterface - Интерфейс геттера групп
type GroupGetterInterface interface {
	// GetNestedGroupsByIds - Получение вложенных групп по переданным ID родителей.
	//
	// По сути в данной библиотеке учитывается, что группы могут иметь вложенную (древовидную) структуру,
	// поэтому для доступных напрямую пользовательских групп необходимо так же получить вложенные подгруппы.
	// Данный метод как раз таки возвращает список подгрупп, доступных пользователю.
	GetNestedGroupsByIds(ctx context.Context, groupId []string) (nestedGroups []string, err error)
}
